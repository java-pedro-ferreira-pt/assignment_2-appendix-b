# Assignment_2 - Appendix B

## Introduction
The application will interact with a PostgreSQL database using JDBC, and the database is called Chinook.

## Requirements
The application must have the following functionality:

- Reading and displaying all customer information from the database.
- Reading and displaying a specific customer by Id or name.
- Adding and updating customers in the database.
- Returning statistics such as the country with the most customers and the highest spending customer.
- Returning a customer's most popular genre.

## Repositories
The repository pattern must be used, with a generic CRUD parent and a customer-specific repository. All functionality should be contained in the customer repository. The repositories should be placed in a repository package and tested using an ApplicationRunner class.

