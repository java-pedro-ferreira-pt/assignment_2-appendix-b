package com.example.MyFirstSpringProject.models;

public record CustomerSpender(String first_name, String last_name) {
}
